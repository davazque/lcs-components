# lcs-components
Some schemas created to be possibly used for the components organization of the
WinCC OA side of LIGHT-CS.

## Brief Overview
The schemas were created to give support to two different kind of XML files:
one to define a single component, its sources and dependencies, and one to
describe a list of components.

The idea is that each component developed for the control system should be named
and somehow versioned to be used in a dependencies system that would allow us to
install only selected components and dependencies in a JCOP installer way.
Thus, components should self-describe themselves in an XML file that could be
stored either directly along with the scripts, or in any other path like a
general components folder. This XML file should contain also information about
all the sources of the component (this way we can reference sources in
different paths like scripts and GUI objects) and also its dependencies.
For the sources, it would be enough to provide the path and optionally a filter.
For the dependencies, to give the name of the component (as defined in its owner
component XML file) and the minimum required version should be enough.

On the other hand, we should have a list of components in a different XML file.
This component list would have to contain simply references to the component XML
files described in the previous paragraph. Alternatively, some simple stuff
could be described inline as well. This would allow us to be able to have a map
of the application components, with their resources and dependencies, that have
to be packed/installed for an application to work. We could have even different
component lists for different applications/subapplications/projects.

All of this should make the life of a packager or dependency tracker program way
easier.

## schemas folder
In the schemas folder you will find 3 different schemas:
* *`lightcs_componenttype.xsd`*: contains the schema for a basic component, its
sources and its dependencies. This is used either for a single component
description or for a components list. This schema is referenced by the other
two but it is not to be used directly to create components or component lists.
* *`lightcs_component.xsd`*: this the schema that you should be using to
describe a single component and its sources and dependencies.
* *`lightcs_componentlist.xsd`*: this is the schema that you should be using to
describe a list of components.

Note that the *`lightcs_componentlist`* schema lets you define the elements of a
component list by either a reference to a component XML (a XML file of the
*`lightcs_component`* schema) or by fully describing a component *inline*. See
the samples for more information.

## samples folder
The samples folder contain two XML examples:
* *`lightcs_componentexample.xml`*: this XML file validates against the
lightcs_component schema and it is an example of how to define a component with
its sources and dependencies.
* *`lightcs_componentlistexample.xml`*: this XML file validantes against the
*`lightcs_componentlist`* schema and will show you some examples of different
ways of defining a component of a list, either by a reference to a
*`lightcs_component`* XML or by fully describing *inline* a component as in a
*`lightcs_component`* XML but within the list.
